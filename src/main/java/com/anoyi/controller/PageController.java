package com.anoyi.controller;

import com.anoyi.tools.JianshuTools;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 *
 * Created by Anoy on 2017/4/21.
 */
@Controller
@AllArgsConstructor
@CrossOrigin("*")
@Log4j2
public class PageController {

	@GetMapping(value = "/")
	public String index(Model model) {
        List<JSONObject> articles = JianshuTools.getCachedArticles();
        model.addAttribute("articles", articles);
        return "index";
	}

	@GetMapping(value = "/chat")
	public String chat() {
        return "chat";
	}

	@GetMapping(value = "/login")
	public String login() {
		return "login";
	}

}
